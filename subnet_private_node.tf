module "subnets-private-node" {
  source                  = "git::https://bitbucket.org/credibilit/terraform-subnets-blueprint.git?ref=3.0.0"
  vpc                     = "${module.vpc.vpc["id"]}"
  cidr_blocks             = "${var.private_node_subnets_cidr_block}"
  azs                     = "${var.azs}"
  prefix                  = "private-node-${var.name}"
  route_tables            = ["${aws_route_table.rt-private-node.*.id}"]
  subnet_count            = "${var.az_count}"
  map_public_ip_on_launch = "false"
  tags                    = "${var.tags}"
}

resource "aws_route_table" "rt-private-node" {
  vpc_id = "${module.vpc.vpc["id"]}"

  tags = "${merge(
    map("Name", "rt-node-${var.name}"),
    var.tags
  )}"

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = "${element(aws_nat_gateway.this.*.id, count.index)}"
  }

  count = "${var.az_count}"
}

output "subnets-private-node" {
  value = "${module.subnets-private-node.ids}"
}
