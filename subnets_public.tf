module "subnets-public" {
  source                  = "git::https://bitbucket.org/credibilit/terraform-subnets-blueprint.git?ref=3.0.0"
  vpc                     = "${module.vpc.vpc["id"]}"
  cidr_blocks             = "${var.public_subnets_cidr_block}"
  azs                     = "${var.azs}"
  prefix                  = "public-${var.name}"
  route_tables            = ["${aws_route_table.rt-public.id}"]
  subnet_count            = "${var.az_count}"
  map_public_ip_on_launch = "false"
  tags                    = "${var.tags}"
}

resource "aws_internet_gateway" "this" {
  vpc_id = "${module.vpc.vpc["id"]}"

  tags = "${merge(
    map("Name", "igw-${var.name}"),
    var.tags
  )}"
}

resource "aws_route_table" "rt-public" {
  vpc_id = "${module.vpc.vpc["id"]}"

  tags = "${merge(
    map("Name", "rt-public-${var.name}"),
    var.tags
  )}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.this.id}"
  }
}

resource "aws_nat_gateway" "this" {
  allocation_id = "${element(aws_eip.eip.*.id, count.index)}"
  subnet_id     = "${module.subnets-public.ids[count.index]}"

  tags = "${merge(
    map(
      "Name", "ngw-public-${var.name}-${substr(var.azs[count.index], -1, 1)}"
    ),
    var.tags
  )}"

  count = "${var.az_count}"
}

resource "aws_eip" "eip" {
  vpc = true

  tags = "${merge(
    map(
      "Name", "eip-public-${var.name}-${substr(var.azs[count.index], -1, 1)}"
    ),
    var.tags
  )}"

  count = "${var.az_count}"
}

output "subnets-public" {
  value = "${module.subnets-public.ids}"
}

output "nat_gateway" {
  value = "${aws_nat_gateway.this.*.id}"
}
