AWS Stack K8S Environment
============================

Creates a network with many core network resourse on AWS, following best practices of kubernetes cluster.
Esscencially this creates:

- A new Virtual Private Clout (VPC) importing "terraform-vpc-blueprint"
- Create a Nat Gateway, Internet Gateway and attach to each public subnet
- Create 3 private subnet, node subnet, master subnet and database subnet.
- For each subnet is associate a route table that give access to nat gateway.

# Sample use

To create the network with this module you need to insert the following peace of code on your own modules, there are lot of other parameters, check :


```
module "test" {
  source                              = "git::https://bitbucket.org/credibilit/stack-network-k8s.git?ref=<VERSION>"
  environment                         = "..."
  name                                = "..."
  domain_name                         = "..."
  cidr_block                          = "..."
  hosted_zone_comment                 = "..."
  tags                                = "..."
  public_subnets_cidr_block           = "..."
  private_node_subnets_cidr_block     = "..."
  private_master_subnets_cidr_block   = "..."
  private_database_subnets_cidr_block = "..."
  azs                                 = "..."
  az_count                            = "..."
}
```

Where `<VERSION>` is the desired version of *this* module. The master branch store the list of versions which can be used. The possible parameters are listed in advance on this document.

## Diagram

![Alt text](https://bitbucket.org/credibilit/stack-network-k8s/raw/f5db8e39f09bbbdd6eee334ff63c3e8f9dcc3ebf/docs/kubernetes%20-%20network.jpeg)

*source image is in folder /docs and It was create on draw.io*

## Compatibilities

The version 2.0.0 and above are created to be used with Terraform 0.11 (possibly its compatible with 0.9, but this was not tested). For version 0.8 use version 0.0.x or the latest version tagged from a commit from 0.8.x branch.

## Input Parameters

The following parameters are used on this module:

- `environment`: "Define witch environment this stack will create all resources"
- `name`: "A name for the VPC and related elements"
- `domain_name`: "The internal DNS domain name for the VPC and DHCP options. required if at least one of them is enabled (default '')"
- `cidr_block`: "The VPC network CIDR Block"
- `public_subnets_cidr_block`: "The list of CIDR blocks for the public subnets. It must match with a subset of the VPC CIDR"
- `private_node_subnets_cidr_block`: "The list of CIDR blocks for the private node subnets. It must match with a subset of the VPC CIDR"
- `private_master_subnets_cidr_block`: "The list of CIDR blocks for the private master subnets. It must match with a subset of the VPC CIDR"
- `private_database_subnets_cidr_block`: "The list of CIDR blocks for the database subnets. It must match with a subset of the VPC CIDR"
- `hosted_zone_comment`: "A comment for the internal hosted zone. This is ignored if custom_route53_zone_id is set (default 'Created by Claranet Brazil VPC blueprint')"
- `tags`: "A map with extra tags (up to 9) to be put in the resources. The Name tag is already handled by the blueprint and should not be referenced here (default {})"
- `azs`: "The list of availability zones to associate the subnets. The sequence will match with the CIDR blocks"
- `az_count`: "How many subnets must be created. This value must be equal or less the CIDR blocks count (default 1)"

## Output parameters

This are the outputs exposed by this module.

- vpc:
    - id: id da VPC
    - cidr_block: VPC CIDR block.
    - cidr_block_bits: bits significativos para o CIDR.
    - private_zone: Route53 private zone id associated to VPC.
    - instance_tenancy: The default instance tenancy set for the VPC.
    - main_route_table_id: The default route table.
    - default_network_acl_id: The VPC default network ACL.
    - default_security_group_id: The default security groups for resources created without it.
    - default_route_table_id: The VPC route table to associate fresh created subnets.
    - network_address: The network classic address notation.
    - network_mask: The network classic mask notation.
- dhcp_options:
    - id: DHCP options id associated with the VPC.
    - domain_name_servers: configured name servers on DHCP options (only used if create a new one).
    - ntp_servers: configured time servers on DHCP options (only used if create a new one).
    - netbios_name_servers: configured netbions name servers on DHCP options (only used if create a new one).
    - netbios_node_type: configured netwbion name servers type on DHCP options (only used if create a new one).
- s3_endpoint:
    - id: The S3 VPC endpoint id.
    - prefix_list: The S3 VPC endpoint prefix list.
- subnets:
    - subnets-private-database-ids: The subnet id of private database.
    - subnets-private-master-ids: The subnet id of servers master of cluster
    - subnets-private-node-ids: The subnet id of servers node of cluster
    - subnets-public-ids: The subnet id of DMZ.
    - nat_gateway: The nat gateway ids
