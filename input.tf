variable "environment" {
  type        = "string"
  description = "Define witch environment this stack will create all resources"
}

variable "name" {
  type        = "string"
  description = "A name for the VPC and related elements"
}

variable "domain_name" {
  type        = "string"
  description = "The internal DNS domain name for the VPC and DHCP options. required if at least one of them is enabled"
  default     = ""
}

variable "cidr_block" {
  type        = "string"
  description = "The VPC network CIDR Block"
}

variable "public_subnets_cidr_block" {
  type        = "list"
  description = "The list of CIDR blocks for the public subnets. It must match with a subset of the VPC CIDR"
}

variable "private_node_subnets_cidr_block" {
  type        = "list"
  description = "The list of CIDR blocks for the private node subnets. It must match with a subset of the VPC CIDR"
}

variable "private_master_subnets_cidr_block" {
  type        = "list"
  description = "The list of CIDR blocks for the private master subnets. It must match with a subset of the VPC CIDR"
}

variable "private_database_subnets_cidr_block" {
  type        = "list"
  description = "The list of CIDR blocks for the database subnets. It must match with a subset of the VPC CIDR"
}

variable "hosted_zone_comment" {
  type        = "string"
  description = "A comment for the internal hosted zone. This is ignored if custom_route53_zone_id is set"
  default     = "Created by Claranet Brazil VPC blueprint"
}

variable "tags" {
  type        = "map"
  description = "A map with extra tags (up to 9) to be put in the resources. The Name tag is already handled by the blueprint and should not be referenced here (default {})"
  default     = {}
}

variable "azs" {
  type        = "list"
  description = "The list of availability zones to associate the subnets. The sequence will match with the CIDR blocks"
}

variable "az_count" {
  type        = "string"
  description = "How many subnets must be created. This value must be equal or less the CIDR blocks count"
  default     = 1
}
