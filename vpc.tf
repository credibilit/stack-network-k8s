module "vpc" {
  source              = "git::https://bitbucket.org/credibilit/terraform-vpc-blueprint.git?ref=2.1.1"
  name                = "${var.name}"
  domain_name         = "${var.name}.local"
  cidr_block          = "${var.cidr_block}"
  hosted_zone_comment = "${var.hosted_zone_comment}"
  tags                = "${var.tags}"
}

output "vpc" {
  value = {
    id                        = "${module.vpc.vpc["id"]}"
    cidr_block                = "${module.vpc.vpc["cidr_block"]}"
    cidr_block_bits           = "${module.vpc.vpc["cidr_block_bits"]}"
    private_zone              = "${module.vpc.vpc["private_zone"]}"
    instance_tenancy          = "${module.vpc.vpc["instance_tenancy"]}"
    main_route_table_id       = "${module.vpc.vpc["main_route_table_id"]}"
    default_network_acl_id    = "${module.vpc.vpc["default_network_acl_id"]}"
    default_security_group_id = "${module.vpc.vpc["default_security_group_id"]}"
    default_route_table_id    = "${module.vpc.vpc["default_route_table_id"]}"
    network_address           = "${module.vpc.vpc["network_address"]}"
    network_mask              = "${module.vpc.vpc["network_mask"]}"
  }
}

output "dhcp_options" {
  value = {
    id                   = "${module.vpc.dhcp_options["id"]}"
    domain_name_servers  = "${module.vpc.dhcp_options["domain_name_servers"]}"
    ntp_servers          = "${module.vpc.dhcp_options["ntp_servers"]}"
    netbios_name_servers = "${module.vpc.dhcp_options["netbios_name_servers"]}"
    netbios_node_type    = "${module.vpc.dhcp_options["netbios_node_type"]}"
  }
}

output "s3_endpoint" {
  value = {
    id          = "${module.vpc.s3_endpoint["id"]}"
    prefix_list = "${module.vpc.s3_endpoint["prefix_list"]}"
  }
}
