module "subnets-private-master" {
  source                  = "git::https://bitbucket.org/credibilit/terraform-subnets-blueprint.git?ref=3.0.0"
  vpc                     = "${module.vpc.vpc["id"]}"
  cidr_blocks             = "${var.private_master_subnets_cidr_block}"
  azs                     = "${var.azs}"
  prefix                  = "private-master-${var.name}"
  route_tables            = ["${aws_route_table.rt-private-master.*.id}"]
  subnet_count            = "${var.az_count}"
  map_public_ip_on_launch = "false"
  tags                    = "${var.tags}"
}

resource "aws_route_table" "rt-private-master" {
  vpc_id = "${module.vpc.vpc["id"]}"

  tags = "${merge(
    map("Name", "rt-master-${var.name}"),
    var.tags
  )}"

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = "${element(aws_nat_gateway.this.*.id, count.index)}"
  }

  count = "${var.az_count}"
}

output "subnets-private-master" {
  value = "${module.subnets-private-master.ids}"
}
